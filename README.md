# PID-app


## Goals

- เข้าใจการเขียนพื้นฐาน javascript, typescript, css, html

## Installation

```bash
yarn
```

## Run Project

```bash
yarn dev
```

## Note

1. เขียน logic ใน ไฟล์ index.ts โดยใช้คำสั่ง `yarn build`(เพื่อ compile code)

2. เขียน css ใน `style.css`

3. html เขียนใน `index.html` หรือจะเขียนใน typescript ก็ได้ (something.innerHtml)
